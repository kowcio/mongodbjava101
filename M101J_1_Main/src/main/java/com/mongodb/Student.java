package com.mongodb;

import java.util.List;

import org.bson.types.ObjectId;
import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Reference;

@Entity("students")
public class Student {
    @Id
    ObjectId id;
    String name;    
    @Embedded
    List<Score> scores;
    
    
    
    
}
