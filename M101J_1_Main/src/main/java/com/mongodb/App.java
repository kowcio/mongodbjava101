package com.mongodb;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.gt;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.bson.Document;
import org.bson.conversions.Bson;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;
import org.mongodb.morphia.query.Query;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import com.mongodb.util.JSON;

public class App {
    public static void main(String[] args) {

        justDoItHomeworkWeek3();

    }

    private static void justDoItHomeworkWeek3() {

        String maindb = "school";
        String collection = "students";

        MongoCollection<Document> coll = getCollectionFromDb(collection, maindb);
       
        // MORPHIA
        /*
         * Morphia morphia = new Morphia();
         * morphia.map(Student.class).map(Score.class); final Datastore ds =
         * morphia.createDatastore(client, "school"); Query<Student> q =
         * ds.createQuery(Student.class); List<Student> students = q.asList();
         * students.forEach(System.out::println);
         */

        Bson filter = Filters.eq("scores.type", "homework");
        Bson sortByScoreAsc = Sorts.orderBy(Sorts.descending("scores.score"));

        Bson fields = Projections.fields(Projections.include("scores"), Projections.excludeId());

        Bson finalFilter = Filters.and(sortByScoreAsc);

        MongoCursor<Document> iterator = coll.find().sort(sortByScoreAsc)
        // .projection(fields)
                .iterator();

        while (iterator.hasNext()) {
            Document doc = iterator.next();
            // Document doc = iterator.next();

            // System.out.println(doc.get("scores").toString());

            String delType = "";
            Double delScore = 100.0;
            Double id =  doc.getDouble("_id");
            List<Document> scoresForId = (ArrayList<Document>) doc.get("scores");
            List<Document> hwScoreDocs = new ArrayList<>();
            for (Document score : scoresForId) {
                
                //System.out.println(doc.get("_id") + " - " + score + " - " + score.getString("type") + " - "+ score.getDouble("score"));
                
                if (score.getString("type").equals("homework")) {
                    delScore = (score.getDouble("score") < delScore) ? score.getDouble("score") : delScore;
                }
            }
            //System.out.println("Lowest homework score found = " + delScore + " id = " + id);

            Bson f = new Document("scores.type", "homework").append("scores.score", delScore);
            Bson f2 = new Document("$pull",f);
            //coll.updateOne(f2);//iterator().forEachRemaining(d->System.out.println("-"+doc.toString()));

            //makes all pretty ;) with the given data !! 
            //selects and updates on PULL every rekord we got here
            Bson sq = new Document("_id", id);
            Bson idoc=new Document("score",delScore).append("type", "homework");
            Bson odoc =new Document("scores",idoc);
            Bson delq=new Document("$pull",odoc);
            coll.updateOne(sq, delq);
            
        }

    }

    private static MongoCollection<Document> getCollectionFromDb(String collection, String maindb) {
        MongoClientOptions opts = MongoClientOptions.builder().connectionsPerHost(10).build();
        MongoClient client = new MongoClient(new ServerAddress(), opts);
        MongoDatabase db = client.getDatabase(maindb).withReadPreference(ReadPreference.secondary());
        MongoCollection<Document> coll = db.getCollection(collection);
        System.out.println(coll.count());
        return coll;
    }

    private static void justDoItHomeworkWeek2() {

        MongoClientOptions opts = MongoClientOptions.builder().connectionsPerHost(10).build();
        MongoClient client = new MongoClient(new ServerAddress(), opts);
        MongoDatabase db = client.getDatabase("students").withReadPreference(ReadPreference.secondary());
        MongoCollection<Document> coll = db.getCollection("grades");
        System.out.println(coll.count());

        Bson gradeHomework = new Document("type", "homework");
        Bson sortByScoreAsc = Sorts.orderBy(Sorts.ascending("student_id"), Sorts.ascending("score"));
        MongoCursor<Document> iterator = coll.find(gradeHomework).sort(sortByScoreAsc).iterator();
        int i = 1;
        Double student_id = null;
        if (coll.count() == 800L)
            while (iterator.hasNext()) {
                Document doc = iterator.next();

                if (!doc.getDouble("student_id").equals(student_id)) {
                    student_id = doc.getDouble("student_id");

                    System.out.println(i++ + " Student id = " + student_id + " doc " + doc.toJson()
                            + "First new row with lower score to delete.");
                    Bson removeLowestStudentScore = new Document("_id", doc.getObjectId("_id"));
                    coll.deleteOne(removeLowestStudentScore);

                    continue;

                }

                student_id = doc.getDouble("student_id");
                System.out.println(i++ + " Student id = " + student_id + " doc " + doc.toJson());

            }

        // coll.find(gradeHomework);

    }

    public static List<Document> importJSONFileToDBUsingJavaDriver(MongoDatabase db, MongoCollection collectionName) {
        // open file
        String pathToFile = "C:\\Users\\Kowcio\\Desktop\\MONGO\\week_2_crud.cedb8431bd5b\\homework_2_3\\grades.json";
        FileInputStream fstream = null;
        List<Document> list = new ArrayList<Document>();
        try {
            fstream = new FileInputStream(pathToFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        try {
            while (br.readLine() != null) {
                System.err.println("BR 1 = " + br.readLine());

                Document bson = (Document) JSON.parse(br.readLine());
                list.add(bson);
            }
            br.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    private static void week2TestingCode() {
        System.out.println("Hello World!");

        MongoClientOptions opts = MongoClientOptions.builder().connectionsPerHost(10).build();

        MongoClient client = new MongoClient(new ServerAddress(), opts);

        MongoDatabase db = client.getDatabase("test").withReadPreference(ReadPreference.secondary());

        MongoCollection<Document> coll = db.getCollection("test");
        // type safety
        // MongoCollection<BsonDocument> coll =
        // db.getCollection("test",BsonDocument.class);

        for (int i = 0; i < 10; i++) {
            Document d = new Document("x", new Random().nextInt(100)).append("y", new Random().nextInt(100)).append(
                    "id", i);
            coll.insertOne(d);
        }

        // Bson filter = new Document("x",new Document("$gt", 90));

        Bson filter = and(eq("x", 0), gt("y", 5));
        Bson projection = new Document("y", 1).append("i", 1).append("_id", 0);
        projection = Projections.exclude("x");
        projection = Projections.include("y");
        projection = Projections.fields(Projections.exclude("_id"), Projections.include("y", "x", "i"));

        Bson sort = new Document("x", 1).append("y", 1);
        sort = Sorts.orderBy(Sorts.ascending("x"), Sorts.descending("y"));
        List<Document> filtereResults = (List<Document>) coll.find().sort(sort).skip(1).limit(5).projection(projection)
                .into(new ArrayList<Document>());
        System.out.println("Filtered:");
        for (Document d : filtereResults)
            System.out.println(d.toJson());

        // BSON document is type safe

        // not type safe but more concise

        Document doc = new Document().append("str", "Mondo DB Hello").append("int", "42").append("null", null)
                .append("list", Arrays.asList(1, 2, 3));

        String str = doc.getString("str");

        System.out.println(doc.toJson());

        System.out.println("Replace:");
        coll.replaceOne(eq("x", 1), new Document("id", 5).append("x", 55).append("update", true));

        System.out.println("Replace:" + coll.find(new Document("id", 5)).into(new ArrayList<Document>()));

        coll.deleteMany(gt("x", 2));
    }
}
