package course;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.result.UpdateResult;

import org.bson.Document;
import org.bson.conversions.Bson;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class BlogPostDAO {
    MongoCollection<Document> postsCollection;

    public BlogPostDAO(final MongoDatabase blogDatabase) {
        postsCollection = blogDatabase.getCollection("posts");
    }

    // Return a single post corresponding to a permalink
    public Document findByPermalink(String permalink) {

        // TODoźj
        // XXX HW 3.2, Work Here
        Document post = new Document("permalink", permalink);
        post = postsCollection.find(post).first();

        return post;
    }

    // Return a list of posts in descending order. Limit determines
    // how many posts are returned.
    public List<Document> findByDateDescending(int limit) {

        // XXX HW 3.2, Work Here
        // Return a list of DBObjects, each one a post from the posts collection
        List<Document> posts = new ArrayList<Document>();

        MongoCursor<Document> iterator = postsCollection.find().sort(Sorts.descending("date")).iterator();
        while (iterator.hasNext()) {
            Document doc = iterator.next();
            posts.add(doc);
        }

        return posts;
    }

    public String addPost(String title, String body, List tags, String username) {

        System.out.println("inserting blog entry " + title + " " + body);

        String permalink = title.replaceAll("\\s", "_"); // whitespace becomes _
        permalink = permalink.replaceAll("\\W", ""); // get rid of non
                                                     // alphanumeric
        permalink = permalink.toLowerCase();

        Document post = new Document("permalink", permalink).append("title", title).append("author", username)
                .append("body", body).append("tags", tags).append("comments", Collections.EMPTY_LIST)
                .append("date", new Date());
        System.out.println("Addind post = " + post.toJson());
        postsCollection.insertOne(post);

        // XXX HW 3.2, Work Here
        // Remember that a valid post has the following keys:
        // author, body, permalink, tags, comments, date
        //
        // A few hints:
        // - Don't forget to create an empty list of comments
        // - for the value of the date key, today's datetime is fine.
        // - tags are already in list form that implements suitable interface.
        // - we created the permalink for you above.

        // Build the post object and insert it

        return permalink;
    }

    // White space to protect the innocent

    // Append a comment to a blog post
    public void addPostComment(final String name, final String email, final String body,
                               final String permalink, String author) {
        
     Document commentToAdd = new Document("author",name)
        .append("name", name );
        if ( (null != email) )
            commentToAdd.append("email", email);
        commentToAdd.append("body", body);
        
        
        System.out.println("Adding comment = " + commentToAdd.toJson());
        UpdateResult res =  postsCollection.updateOne(
                Filters.eq("permalink",permalink),
                new Document("$push" , 
                        new Document("comments",commentToAdd))  );
        //System.out.println("Comment push = " + res.toString());

        // XXX HW 3.3, Work Here
        // Hints:
        // - email is optional and may come in NULL. Check for that.
        // - best solution uses an update command to the database and a suitable
        //   operator to append the comment on to any existing list of comments
    }
}
